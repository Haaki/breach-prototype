﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DungeonBuilder : MonoBehaviour
{
    public List<GameObject> roomsPrefabs;
    public GameObject bossRoom;
    public List<GameObject> keysPrefabs;
    public List<Room> rooms = new List<Room>();


    public int minRooms;
    public int deadEndRetries;
    public int keyFrequency = 1;

    public Valve.VR.InteractionSystem.Teleport teleporter;

    Room etranceRoom;
    
    private void Awake()
    {
        etranceRoom = GetComponent<Room>();
    }
    private void Start()
    {
        Generate();
        FixTeleporting();
    }
    void Generate()
    {
        Expand(minRooms);
        Room bRoom = PlaceRoom(Instantiate(bossRoom).GetComponent<Room>());
        bRoom.name += "(Boss)";
        bRoom.links[0].connectedRoom = bRoom;
        Expand(etranceRoom.AllOpenLinks().Count);
        PlaceKey(bRoom.links[0], rooms[rooms.Count - 1]);

        foreach (Room r in rooms)
            r.bounds.enabled = false;
        foreach (Link l in etranceRoom.AllOpenLinks())
            l.exit.DeadEnd();
    }
    void FixTeleporting()
    {
        List<Valve.VR.InteractionSystem.TeleportMarkerBase> teleportAreas = new List<Valve.VR.InteractionSystem.TeleportMarkerBase>() {transform.Find("TPArea").GetComponent<Valve.VR.InteractionSystem.TeleportMarkerBase>() };

        foreach (Valve.VR.InteractionSystem.TeleportMarkerBase tpmb in FindObjectsOfType<Valve.VR.InteractionSystem.TeleportArea>())
        {
            if (!tpmb.transform.parent.name.Contains("(Clone)"))
            {
                teleportAreas.Add(tpmb);
                tpmb.name += tpmb.transform.parent.name;
            }
        }
        FindObjectOfType<Valve.VR.InteractionSystem.Teleport>().teleportMarkers = teleportAreas.ToArray();
    }
    void Expand(int roomCount)
    {
        int tries = 0;  //  Failsafe
        int targetRoomCount = rooms.Count + roomCount;
        while (rooms.Count < targetRoomCount)
        {
            List<Link> openLinks = HaakiUtils.RandGen.Shuffle(etranceRoom.AllOpenLinks());
            List<GameObject> egliable = roomsPrefabs.FindAll(a => a.GetComponent<Room>().exits.Exists(b => b.size == openLinks[0].exit.size));
            if (egliable.Count > 0)
            {
                Room newRoom = Instantiate(HaakiUtils.RandGen.Shuffle(egliable)[0])?.GetComponent<Room>();

                PlaceRoom(newRoom, rooms.Count % keyFrequency == 0 || rooms.Count + 1 == targetRoomCount);
            }

            if (++tries > 3 * targetRoomCount)
            {
                Debug.LogWarning("Got stuck creating rooms! :(");
                UnityEngine.SceneManagement.SceneManager.LoadScene(0);
                break;
            }
        }
        Debug.Log($"Failed {tries - roomCount} times");
    }
    Room PlaceRoom(Room newRoom, bool locked = false)
    {
        foreach (Link l1 in HaakiUtils.RandGen.Shuffle(etranceRoom.AllOpenLinks(new List<Room>())))
            foreach (Link l2 in HaakiUtils.RandGen.Shuffle(newRoom.links.FindAll(a => a.exit.size == l1.exit.size)))
            {
                newRoom.transform.rotation = Quaternion.Euler((l1.exit.transform.rotation * Quaternion.Euler(0, 180, 0)).eulerAngles - l2.exit.transform.localRotation.eulerAngles);
                if (!((l1.exit.transform.rotation * Quaternion.Euler(0, 180, 0)).eulerAngles.y - l2.exit.transform.rotation.eulerAngles.y < 0.005f)) Debug.LogError("Aaaaaah! math is wrong! Room rotation calculation failed! Angles are hard!");

                newRoom.transform.position = l1.exit.transform.position + (newRoom.transform.position - l2.exit.transform.position);
                bool overlaps = false;
                foreach (Room r in rooms)
                    if (r != etranceRoom && Room.Intersects(r, newRoom))
                        overlaps = true;

                if (!overlaps)
                {
                    newRoom.name = $"Room{rooms.Count}";
                    newRoom.transform.parent = transform.parent;
                    rooms.Add(newRoom);
                    l1.connectedRoom = l2.parent;
                    l2.connectedRoom = l1.parent;
                    if (locked)
                        PlaceKey(l1);
                    return newRoom;
                }
            }
        Destroy(newRoom.gameObject);
        return null;
    }
    void PlaceKey(Link link, Room target = null)
    {
        link.exit.door = true;
        Key key = Instantiate(keysPrefabs.Find(a => a.GetComponent<Key>().type == link.exit.lockType)).GetComponent<Key>();
        if (target == null)
            target = HaakiUtils.RandGen.Shuffle(rooms.FindAll(a => a != rooms[0] && a != link.connectedRoom))[0];
        key.transform.position = target.transform.Find("Reflection Probe").transform.position;
        Debug.Log($"{link.parent.name} enabled door {link.exit}, key placed in {target}");
    }
}
