﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

namespace HaakiUtils
{
    public class RandGen
    {
        public static void Seed(int seed)
        {
            UnityEngine.Random.InitState(seed);
        }
        //  Returns a float value between 0 and 1
        public static float RandomValue()
        {
            return UnityEngine.Random.value;
        }
        //  Returns a Vector3 with random components between -1 and 1
        public static Vector3 RandomVector3()
        {
            return new Vector3(RandomValue() - 0.5f, RandomValue() - 0.5f, RandomValue() - 0.5f) * 2;
        }
        //  Returns a Vector3 with random components between -magnitude and magnitude
        public static Vector3 RandomVector3(float magnitude)
        {
            return new Vector3(RandomValue() - 0.5f, RandomValue() - 0.5f, RandomValue() - 0.5f) * 2 * magnitude;
        }
        //  Returns a Vector3 with random components between -magnitude and magnitude
        public static Vector3 RandomVector3(Vector3 magnitude)
        {
            return new Vector3((RandomValue() - 0.5f) * magnitude.x, (RandomValue() - 0.5f) * magnitude.y, (RandomValue() - 0.5f) * magnitude.z) * 2;
        }
        //  Returns true chance% of the time
        public static bool PercentRandom(float chance)
        {
            return RandomValue() < chance / 100.0f;
        }
        //  Returns true num / denom of the time
        public static bool FractionRandom(int denominator, int numerator = 1)
        {
            return RandomValue() < (float)numerator / denominator;
        }
        //  Randomizes the x value of a vector using y as a +/- variation
        public static void Randomize(ref Vector2 value)
        {
            value.x = value.x + (RandomValue() * 2 - 1) * value.y;
        }
        public static void Randomize(ref Vector2Int value)
        {
            value.x = value.x + (int)((RandomValue() * 2 - 1) * value.y);
        }
        //  Same as the last two but leaves the original vector intact
        public static Vector2 Generate(Vector2 value)
        {
            return new Vector2(value.x + (RandomValue() * 2 - 1) * value.y, value.y);
        }
        public static Vector2Int Generate(Vector2Int value)
        {
            return new Vector2Int(value.x + (int)((RandomValue() * 2 - 1) * value.y), value.y);
        }
        //  Shuffles a list
        public static List<T> Shuffle<T>(List<T> list)
        {
            return list.OrderBy(a => RandomValue()).ToList();
        }
    }

    //-------------------------------------------------------------------------------------
    //  Unfinished weighted list implementation TODO: Finish implementing weighted lists
    public class WeightedList<T> : List<T>, ISerializationCallbackReceiver
    {
        public List<T> Values;
        public List<float> Weights;
        [HideInInspector]
        public List<float> weights;

        public void OnBeforeSerialize()
        {
            if (Values == null) Values = new List<T>();
            if (Weights == null) Weights = new List<float>();
            Values.Clear();
            Weights.Clear();
            Values = new List<T>(this);
            Weights = new List<float>(weights);
        }
        public void OnAfterDeserialize()
        {
            Clear();
            AddRange(Values, Weights);
        }
        #region List<T> overrides
        public WeightedList()
        {
            weights = new List<float>();
        }
        public WeightedList(List<T> v)
        {
            AddRange(v);
        }
        //  Constructor that allows passing a list of weights
        public WeightedList(List<T> v, List<float> w)
        {
            AddRange(v, w);
        }
        public new void Add(T item)
        {
            Add(item, 1);
        }
        //  Add that allows passing a weight
        public void Add(T item, float weight)
        {
            weights.Add(weight);
            (this as List<T>).Add(item);
        }
        public new void AddRange(IEnumerable<T> items)
        {
            foreach (T item in items)
                Add(item);
        }
        //  AddRange that allows passing a list of weights
        public void AddRange(IEnumerable<T> items, IEnumerable<float> w)
        {
            for (int i = 0; i < items.Count(); i++)
                if (i < w.Count())
                    Add((items as List<T>)[i], (w as List<float>)[i]);
                else
                    Add((items as List<T>)[i]);
        }
        public new void Clear()
        {
            weights.Clear();
            (this as List<T>).Clear();
        }
        public new void Remove(T item)
        {
            weights.RemoveAt(IndexOf(item));
            (this as List<T>).Remove(item);
        }
        public new void RemoveAll(Predicate<T> predicate)
        {
            List<T> toRemove = FindAll(predicate);
            foreach (T item in toRemove)
                weights.RemoveAt(IndexOf(item));
            (this as List<T>).RemoveAll(predicate);
        }
        public new void RemoveAt(int index)
        {
            weights.RemoveAt(index);
            (this as List<T>).RemoveAt(index);
        }
        public new void RemoveRange(int index, int count)
        {
            weights.RemoveRange(index, count);
            (this as List<T>).RemoveRange(index, count);
        }
        public new void Reverse()
        {
            weights.Reverse();
            (this as List<T>).Reverse();
        }
        public new void Reverse(int index, int count)
        {
            weights.Reverse(index, count);
            (this as List<T>).Reverse(index, count);
        }
        public new void Sort()
        {
            Debug.LogWarning("Please tell me why you would want to sort a weighted list, and i might actually try to implement it...");
        }
        public new void Sort(Comparison<T> comparison)
        {
            Debug.LogWarning("Please tell me why you would want to sort a weighted list, and i might actually try to implement it...");
        }
        public new void Sort(IComparer<T> comparer)
        {
            Debug.LogWarning("Please tell me why you would want to sort a weighted list, and i might actually try to implement it...");
        }
        public new void Sort(int index, int count, IComparer<T> comparer)
        {
            Debug.LogWarning("Please tell me why you would want to sort a weighted list, and i might actually try to implement it...");
        }
#endregion
        
        public T GetRandomValue()
        {
            List<T> pile = new List<T>();
            for (int i = 0; i < Count; i++)
                for (int j = 0; j < weights[i]; j++)
                    pile.Add(this[i]);
            return RandGen.Shuffle(pile)[0];
        }
    }
    [Serializable] public class WeightedGameobjectList : WeightedList<GameObject> { }

}
