﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Key : MonoBehaviour
{
    public enum KeyType { small, large, boss }
    public KeyType type;
    public bool used;
    public GameObject pedestal;
    public Vector3 keyholeOffset;

    private void Start()
    {
        if (pedestal == null)
        {
            GetComponent<Rigidbody>().AddForce(HaakiUtils.RandGen.RandomVector3() * 100);
            GetComponent<Rigidbody>().AddTorque(HaakiUtils.RandGen.RandomVector3() * 100);
        }
    }
}
