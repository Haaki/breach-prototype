﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    DungeonBuilder db;
    public List<Exit> exits;
    public List<Link> links = new List<Link>();
    public BoxCollider bounds;

    private void Awake()
    {
        db = FindObjectOfType<DungeonBuilder>();
        foreach (Exit e in exits)
        {
            links.Add(new Link() {parent = this, exit =  e});
        }
        bounds.size *= 0.99f;   //  I hate floating point errors...
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.DrawWireCube(bounds.center, bounds.size);
    }

    //  Finds all open links of this room
    public List<Link> OpenLinks()
    {
        return links.FindAll(a => a.connectedRoom == null);
    }
    //  Finds all open links of this an linked rooms
    public List<Link> AllOpenLinks()
    {
        return AllOpenLinks(new List<Room>());
    }
    public List<Link> AllOpenLinks(List<Room> closedRooms)
    {
        List<Link> openLinks = OpenLinks();
        closedRooms.Add(this);
        foreach (Link closedLink in links.FindAll(a => a.connectedRoom != null))
            if (!closedRooms.Contains(closedLink.connectedRoom))
                openLinks.AddRange(closedLink.connectedRoom.AllOpenLinks(closedRooms));

        return openLinks;
    }
    //  Gets the link of a random open exit
    Link OpenLink()
    {
        List<Link> openLinks = OpenLinks();
        if (openLinks.Count > 0)
            return HaakiUtils.RandGen.Shuffle(openLinks)[0];
        else
        {
            Debug.LogWarning($"Warning: No open exits found for {gameObject.name}");
            return null;
        }
    }
    //  Checks if a point in space is within the bounds of this room
    public bool Contains(Vector3 point)
    {
        bool b =
        Vector3.Dot((point - (transform.position + (transform.rotation * bounds.center) + transform.right * (bounds.size.x / 2.0f))), transform.right) <= 0.001f &&       //  Right face
        Vector3.Dot((point - (transform.position + (transform.rotation * bounds.center) - transform.right * (bounds.size.x / 2.0f))), -transform.right) <= 0.001f &&     //  Left face
        Vector3.Dot((point - (transform.position + (transform.rotation * bounds.center) + transform.up * (bounds.size.y / 2.0f))), transform.up) <= 0.001f &&             //  Up face
        Vector3.Dot((point - (transform.position + (transform.rotation * bounds.center) - transform.up * (bounds.size.y / 2.0f))), -transform.up) <= 0.001f &&           //  Down face
        Vector3.Dot((point - (transform.position + (transform.rotation * bounds.center) + transform.forward * (bounds.size.z / 2.0f))), transform.forward) <= 0.001f &&   //  Front face
        Vector3.Dot((point - (transform.position + (transform.rotation * bounds.center) - transform.forward * (bounds.size.z / 2.0f))), -transform.forward) <= 0.001f;   //  Back face
        
        return b;
    }
    //  Checks if another room intersects this room
    public bool Intersects(Room otherRoom)
    {
        //  Returns true automatically if the distance between the centers is smaller than the smallest dimention of the bounds
        float distance = ((transform.position + transform.rotation * bounds.center) - (otherRoom.transform.position + otherRoom.transform.rotation * otherRoom.bounds.center)).magnitude;
        if (distance < Mathf.Min(new float[] {bounds.size.x,bounds.size.y,bounds.size.z, otherRoom.bounds.size.x, otherRoom.bounds.size.y, otherRoom.bounds.size.z }))
            return true;

        Vector3[] points = new Vector3[15];
        
        //  8 corners
        points[0] = points[1] = points[2] = points[3] = points[4] = points[5] = points[6] = points[7] = transform.position + (transform.rotation * (bounds.size / 2));
        points[0].x = points[1].x = points[2].x = points[3].x *= -1;
        points[0].y = points[1].y = points[4].y = points[5].y *= -1;
        points[0].z = points[2].z = points[4].z = points[6].z *= -1;
        //  6 sides
        points[8] = (transform.position + (transform.rotation * bounds.center) + transform.right * (bounds.size.x / 2.0f));
        points[9] = (transform.position + (transform.rotation * bounds.center) - transform.right * (bounds.size.x / 2.0f));
        points[10] = (transform.position + (transform.rotation * bounds.center) + transform.up * (bounds.size.y / 2.0f));
        points[11] = (transform.position + (transform.rotation * bounds.center) - transform.up * (bounds.size.y / 2.0f));
        points[12] = (transform.position + (transform.rotation * bounds.center) + transform.forward * (bounds.size.z / 2.0f));
        points[13] = (transform.position + (transform.rotation * bounds.center) - transform.forward * (bounds.size.z / 2.0f));
        // center
        points[14] = transform.position + (transform.rotation * bounds.center);
        //  Check each point if it is inside the other room
        bool b = false;
        foreach (Vector3 point in points)
            if (otherRoom.Contains(point + transform.rotation * bounds.center))
                b = true;
        return b;
    }
    //  Checks if two rooms intersects eachother
    public static bool Intersects(Room r1, Room r2)
    {
        return r1.Intersects(r2) || r2.Intersects(r1);
    }
}

[System.Serializable]
public class Link
{
    [HideInInspector]
    public Room parent;
    public Exit exit;
    public Room connectedRoom;
}