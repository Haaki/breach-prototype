﻿using UnityEngine;
using Valve.VR;


public class Player : MonoBehaviour
{
    public Camera eyes;
    public Transform lFlashlight;
    public Transform rFlashlight;
    public float shoesize = 1;
    public float walkspeed = 1;
    public float height = 2;
    
    private void Start()
    {
        height -= shoesize * 2 + 0.1f;
    }

    private void Update()
    {
        if (SteamVR_Actions._default.Flashlight.GetLastStateDown(SteamVR_Input_Sources.Any))
        {
            lFlashlight.gameObject.SetActive(rFlashlight.gameObject.activeSelf);
            rFlashlight.gameObject.SetActive(!lFlashlight.gameObject.activeSelf);
        }

        if (Input.GetKeyDown(KeyCode.R))
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        if (Input.GetKeyDown(KeyCode.Escape))
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
    }
    private void FixedUpdate()
    {
        //  Code for walking, unused because teleporting is more fun
        /**
        if (SteamVR_Actions._default.Walk.GetState(SteamVR_Input_Sources.Any))
        {
            Vector2 trackPadPos = SteamVR_Actions._default.Movement.GetAxis(SteamVR_Input_Sources.Any);
            Vector3 walkoffset = lHand.forward * trackPadPos.y + lHand.right * trackPadPos.x;
            walkoffset.y = 0;
            if (!Physics.Raycast(new Ray(eyes.transform.position, walkoffset), out RaycastHit WallHit, walkoffset.magnitude * walkspeed))
            {
                transform.position += walkoffset * walkspeed;
            }

        }
        /**/
        if (Physics.Raycast(new Ray(eyes.transform.position, Vector3.down), out RaycastHit floorHit, 10))
        {
            if(floorHit.collider.tag == "Floor")
                transform.position = new Vector3(transform.position.x, floorHit.point.y, transform.position.z);
        }
    }
}
