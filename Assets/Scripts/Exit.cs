﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exit : MonoBehaviour
{
    public Vector2Int size;
    public bool door;
    public bool opening;
    public Key.KeyType lockType;
    public Transform[] doors = new Transform[2];
    private void Awake()
    {
        transform.Find("DeadEnd").gameObject.SetActive(false);
        transform.Find("Arrow").gameObject.SetActive(false);    //  Hide the Connection arrow
    }
    private void Start()
    {
        transform.Find("Door").gameObject.SetActive(door);
        GetComponent<Collider>().enabled = door;
    }
    private void Update()
    {
        if (opening)
        {
            switch (lockType)
            {
                case Key.KeyType.small:
                    doors[0].localRotation = Quaternion.Euler(0, doors[0].localRotation.eulerAngles.y - (1 - (doors[0].localRotation.eulerAngles.y / -90)) * Time.deltaTime * 10, 0);
                    if (doors[0].localRotation.eulerAngles.y < 270)
                        opening = false;
                    break;
                case Key.KeyType.large:
                    doors[0].localRotation = Quaternion.Euler(0, doors[0].localRotation.eulerAngles.y - (1 - (doors[0].localRotation.eulerAngles.y / -90)) * Time.deltaTime * 10, 0);
                    doors[1].localRotation = Quaternion.Euler(0, doors[1].localRotation.eulerAngles.y + (1 - (doors[0].localRotation.eulerAngles.y / -90)) * Time.deltaTime * 10, 0);
                    if (doors[0].localRotation.eulerAngles.y < 270 || doors[1].localRotation.eulerAngles.y > 90)
                        opening = false;
                    break;
                case Key.KeyType.boss:
                    doors[0].localRotation = Quaternion.Euler(doors[0].localRotation.eulerAngles.x + 2 * Time.deltaTime * 10,0,0);
                    doors[0].localPosition += Vector3.forward * Time.deltaTime;
                    break;
                default:
                    break;
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        Key key = other.transform.parent.GetComponent<Key>();
        if (key?.type == lockType && key?.used != true)
        {
            Debug.Log("Sesame!");
            opening = true;
            GetComponent<Collider>().enabled = false;
            key.used = true;
            key.GetComponentInParent<Valve.VR.InteractionSystem.Hand>()?.DetachObject(key.gameObject);
            Destroy(key.GetComponentInParent<Valve.VR.InteractionSystem.Throwable>());
            key.GetComponentInParent<Valve.VR.InteractionSystem.Throwable>().enabled = false;
            key.GetComponent<Rigidbody>().isKinematic = true;
            key.transform.parent = doors[0];
            key.transform.localPosition = key.keyholeOffset;
            key.transform.localRotation = Quaternion.Euler(90, 0, 90);
            if (key.pedestal != null) Destroy(key.pedestal);
        }
    }
    public void DeadEnd()
    {
        transform.Find("DeadEnd").gameObject.SetActive(true);
        transform.Find("Door").gameObject.SetActive(false);
    }
}
