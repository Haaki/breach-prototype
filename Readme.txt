Prototype dungeon explorer
by Haakon Aarstein

Use keys found around the dungeon to open doors in searh of the ultimate door; the Boss door, of course requiring a Boss key to open.
Good luck! Try not to get lost.

Only tested using HTC Vive, should work on anything that can use Steam VR , but the controls might not be set up properly.

Controls (Vive):
Triggers to grab
Trackpad to teleport
Menu button to swap wich had the flashlight is in

Keyboard shortcuts:
esc to quit the game
r to restart

There is an issue where rooms generate inside eachother, if that happens just restart.

online repo: https://bitbucket.org/Haaki/breach-prototype/src/master/

To set up project:
Create new unity project. (version 2019.1.0b2)
Paste the repo into the project folder.
Launch unity, and open the project.

To set up controls for other headsets:
make sure the headset and controllers are connected
go to Window -> SteamVr Input
click "Open binding UI"
open your desired controller, and click the "+" next to the buttons to add a keybind.
make sure Grab Grip, flashlight and teleport is bound.
click "Replace default binding"